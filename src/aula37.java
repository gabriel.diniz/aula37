public class aula37 { // break

    public static void main(String[] args) {

  // looping
    /*for(int i=0; i<10; i++){ // i variavel 0, i menor que 10, acrescenta uma unidade
        if(true) // se verdadeiro
            break;  // encerra
        }
    */
        // looping while
        /*
        int i = 10; // variavel i valor 10
        while(i<100) { // executa enquanto i for menor que 100
            i++; // acrescenta uma unidade
            if(true)  // verifica se é verdadeira
                break; // encerra
        }
        */

        // instrução switch
        /*
        int dia = 2;  // variavel inteira dia valor 2
        switch (dia){  // instrução switch variavel dia
            case 1:  // caso valor contido valor 1 executa o bloco de instrução
                System.out.println("Domingo"); // imprime mensagem
            break;  // encerra
            case 2: // caso valor contido valor 2 executa o bloco de instrução
                System.out.println("Segunda"); // imprime mensagem
                break;// encerra
            default: // caso valor contido valor default executa o bloco de instrução
                System.out.println("Dia inválido"); // imprime mensagem
        }
         */

        // bloco de instrução

        bloco1: {  // bloco com label
            System.out.println("1");  // imprimi o valor 1
            if (true) //se for verdadeiro
            break bloco1; // interromper bloco 1
            System.out.println("5"); //imprimi o valor 5
        }

    }
}
